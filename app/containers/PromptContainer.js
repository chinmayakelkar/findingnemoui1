var React = require('react');


var PromptContainer = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  }, 
  getInitialState: function () {
    return {
      username: ''
    }
  },
  onSubmitUser:function(e){
	e.preventDefault();
	var username=this.state.username;
	this.setState({
		username:''
	});
  },
  onUpdateUser:function(e){
	this.setState({
		username:e.target.value
	}
	);  
  },
  render: function () {
    return (
	<div className="section text-left">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1 className="text-center">Add Details</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <form role="form" onSubmit={this.onSubmitUser}>
              <div className="form-group">
                <label className="control-label" htmlFor="fullName1">Full Name</label>
                <input className="form-control" id="fullName" placeholder="Enter Full Name"
                type="text" onChange={this.onUpdateUser} value={this.state.username}/>
              </div>
              <div className="form-group">
                <label className="control-label" htmlFor="exampleInputEmail1">Email address</label>
                <input className="form-control" id="exampleInputEmail1"
                placeholder="Enter email" type="email"/>
              </div>
              <div className="form-group">
                <label className="control-label" htmlFor="telephoneNo">Mobile Number</label>
                <input className="form-control" id="telephoneNo" placeholder="Mobile Number"
                type="tel"/>
              </div>
              <div className="form-group">
                <label className="control-label" htmlFor="date">Date</label>
                <input className="form-control" id="date" placeholder="Date" type="date"/>
              </div>
              <div className="form-group">
                <label className="control-label" htmlFor="file">Image</label>
                <input className="form-control" id="file" placeholder="Image"
                type="file"/>
              </div>
              <button type="submit" className="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
  }
});

module.exports = PromptContainer;
